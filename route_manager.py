class RouteManager:

    def __init__(self, graph=None):
        # initialize graph object
        if graph == None:
            graph = {}
        self.__graph = graph

    def connected(self, city1, city2):
        # stack search through our graph to find all cities connected to city1
        graph = self.__graph
        if city1 not in graph or city2 not in graph:
            raise ValueError("city not added")
        visited, stack = set(), [city1]
        while stack:
            city = stack.pop()
            if city not in visited:
                visited.add(city)
                stack.extend(graph[city] - visited)  # add subset to stack minus visited
        # check to see if city2 is in resulting list
        if city2 in visited:
            return True
        else:
            return False

    def get_route(self, city1, city2):
        if self.connected(city1, city2) == False:
            raise ValueError("city pair not connected")
        # queue data structure
        graph = self.__graph
        queue = [(city1, [city1])]
        while queue:
            (city, route) = queue.pop(0)
            for next in graph[city] - set(route):
                if next == city2:
                    return route + [next]
                else:
                    queue.append((next, route + [next]))

    def add_connection(self, city1, city2):
        # if city key exists append other city, if not create new entry
        if city1 in self.__graph:
            self.__graph[city1].add(city2)
        else:
            self.__graph[city1] = set([city2])
        if city2 in self.__graph:
            self.__graph[city2].add(city1)
        else:
            self.__graph[city2] = set([city1])
        #print(self.__graph)

    def cities(self):
        # returns cities
        return set(self.__graph.keys())

    def return_graph(self):
        return self.__graph