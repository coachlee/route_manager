from unittest import TestCase
from route_manager import RouteManager
import csv

class TestRouteManager(TestCase):

    def test_add_connection(self):
        r = RouteManager()
        # test that add works
        r.add_connection("Washington", "Philadelphia")
        self.assertTrue("Washington" in r.return_graph())
        self.assertTrue("Philadelphia" in r.return_graph())
        self.assertTrue("New York" not in r.return_graph())
        # insure additional add does not affect previous data
        r.add_connection("Philadelphia", "Newark")
        self.assertTrue("Washington" in r.return_graph())
        self.assertTrue("Philadelphia" in r.return_graph())
        self.assertTrue("Newark" in r.return_graph())
        self.assertTrue("New York" not in r.return_graph())
        # insure adding same cities does not change graph
        graph = dict(r.return_graph())
        r.add_connection("Philadelphia", "Newark")
        self.assertTrue(r.return_graph() == graph)

    def test_cities(self):
        # testing returning of unique cities in graph
        r = RouteManager()
        expect_set = set(["Washington", "Philadelphia", "Newark"])
        r.add_connection("Washington", "Philadelphia")
        r.add_connection("Philadelphia", "Newark")
        self.assertTrue(expect_set == r.cities())

    def test_connected(self):
        r = RouteManager()
        r.add_connection("Washington", "Philadelphia")
        r.add_connection("Philadelphia", "Newark")
        r.add_connection("Newark", "New York")
        r.add_connection("Tokyo", "Hong Kong")
        # Tokyo and HK should be discreet from the rest of the cities
        self.assertTrue(r.connected("Washington", "Philadelphia"))
        self.assertTrue(r.connected("Washington", "Newark"))
        self.assertTrue(r.connected("Washington", "New York"))
        self.assertTrue(r.connected("Philadelphia", "New York"))
        self.assertFalse(r.connected("Tokyo", "New York"))
        self.assertTrue(r.connected("Tokyo", "Hong Kong"))
        # should all be connected after this add_connection
        r.add_connection("New York", "Hong Kong")
        self.assertTrue(r.connected("Tokyo", "New York"))
        self.assertTrue(r.connected("Tokyo", "Newark"))
        # should raise no city error
        self.assertRaises(ValueError, lambda: r.connected("Paris", "New York"))

    def test_get_route(self):
        r = RouteManager()
        # read from csv
        with open('city_list.csv', 'r') as f:
            reader = csv.reader(f)
            city_list = list(reader)
        # add list to graph
        for (x, y) in city_list:
            r.add_connection(x,y)
        # check a couple of routes of different lengths
        self.assertTrue(r.get_route("Atlanta", "Washington") == ['Atlanta', 'Charlotte', 'Richmond', 'Washington'])
        self.assertTrue(r.get_route("Charlotte", "New York") == ['Charlotte', 'Pittsburgh', 'New York'])
        # check the inverse route
        self.assertTrue(r.get_route("New York", "Charlotte") == ['New York', 'Pittsburgh', 'Charlotte'])
        # check a route right next to each other
        self.assertTrue(r.get_route("Cleveland", "Detroit") == ['Cleveland', 'Detroit'])
        # check to see that we handle not added cities
        self.assertRaises(ValueError, lambda: r.get_route("Taipei", "Detroit"))
        # check to see that we handle unconnected cities
        self.assertRaises(ValueError, lambda: r.get_route("San Diego", "Detroit"))